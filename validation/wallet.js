const validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = data => {
  let errors = {};

  data.wallet = !isEmpty(data.wallet) ? data.wallet : "";

  if (validator.isEmpty(data.wallet)) {
    errors.wallet = "กรุณาระบุชื่อกระเป๋าเงิน";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
