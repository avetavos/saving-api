const validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = data => {
  let errors = {};

  data.type = !isEmpty(data.type) ? data.type : "";
  data.date = !isEmpty(data.date) ? data.date : "";
  data.amount = !isEmpty(data.amount) ? data.amount : "";

  if (validator.isEmpty(data.type)) {
    errors.type = "กรุณาระบุประเภทธุรกรรม";
  }
  if (validator.isEmpty(data.date)) {
    errors.date = "กรุณาระบุวันที่ทำธุรกรรม";
  }
  if (validator.isEmpty(data.amount)) {
    errors.amount = "กรุณาระบุจำนวนเงิน";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
