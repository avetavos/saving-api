const router = require("express").Router();
const bcrypt = require("bcryptjs");
const keys = require("../config/keys");
const passport = require("passport");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");

const User = require("../models/User");

const validateRegister = require("../validation/register");
const validateLogin = require("../validation/login");

// Route::: POST /user/register
// Desc::: Register new user
// Access::: Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegister(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = "อีเมลนี้มีบัญชีอยู่แล้ว";
      return res.status(400).json(errors);
    }

    const avatar = gravatar.url(req.body.email, {
      s: "200",
      r: "pg",
      d: "mm"
    });

    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      avatar
    });

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(req.body.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser
          .save()
          .then(user => res.status(201).json(user))
          .catch(err => console.log(err));
      });
    });
  });
});

// Route::: POST /user/login
// Desc::: Login user and return token
// Access::: Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLogin(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const { email, password } = req.body;

  User.findOne({ email }).then(user => {
    if (!user) {
      errors.email = "อีเมลนี้ยังไม่ได้ทำการลงทะเบียน";
      return res.status(404).json(errors);
    }

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        const payload = { _id: user._id, name: user.name, avatar: user.avatar };
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: `Bearer ${token}`
            });
          }
        );
      } else {
        errors.password = "รหัสผ่านไม่ถูกต้อง";
        return res.status(400).json(errors);
      }
    });
  });
});

// Route::: POST /user/current
// Desc::: Get current user
// Access::: Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user._id,
      name: req.user.name,
      email: req.user.email
    });
  }
);

module.exports = router;
