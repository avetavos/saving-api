const router = require("express").Router();
const bcrypt = require("bcryptjs");
const keys = require("../config/keys");
const passport = require("passport");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const User = require("../models/User");
const Wallet = require("../models/Wallet");

const validateWallet = require("../validation/wallet");
const validateTransaction = require("../validation/transaction");

// Route::: POST /wallet/
// Desc::: Create new wallet
// Access::: Private
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateWallet(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newWallet = new Wallet({
      user: req.user._id,
      name: req.body.wallet
    });

    newWallet.save().then(wallet => res.status(201).json(wallet));
  }
);

// Route::: GET /wallet/dashboard
// Desc::: Get all wallets of current user
// Access::: Private
router.get(
  "/dashboard",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Wallet.find({ user: req.user._id })
      .then(wallets => {
        return res.status(400).json(wallets);
      })
      .catch(err => console.log(err));
  }
);

// Route::: GET /wallet/:wallet_id
// Desc::: Get wallet by id of current user
// Access::: Private
router.get(
  "/:wallet_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Wallet.findById(req.params.wallet_id)
      .then(wallet => {
        return res.status(400).json(wallet);
      })
      .catch(err => console.log(err));
  }
);

// Route::: PUT /wallet/:wallet_id
// Desc::: Edit wallet by id
// Access::: Private
router.put(
  "/:wallet_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateWallet(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const editWallet = {
      name: req.body.wallet
    };

    Wallet.findByIdAndUpdate(
      req.params.wallet_id,
      editWallet,
      { new: true },
      (err, wallet) => {
        if (err) throw err;
        return res.status(400).json({
          success: true,
          wallet: wallet
        });
      }
    );
  }
);

// Route::: DELETE /wallet/:wallet_id
// Desc::: Delete wallet by id
// Access::: Private
router.delete(
  "/:wallet_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Wallet.findByIdAndRemove(req.params.wallet_id, (err, wallet) => {
      if (err) throw err;
      return res.status(400).json({
        success: true,
        wallet: wallet
      });
    });
  }
);

// Route::: POST /wallet/:wallet_id/expenses
// Desc::: Create expenses list
// Access::: Private
router.post(
  "/:wallet_id/expenses",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newExpense = req.body;

    Wallet.findByIdAndUpdate(
      req.params.wallet_id,
      { $push: { expenses: newExpense } },
      { new: true },
      (err, wallet) => {
        if (err) throw err;
        return res.status(400).json({
          wallet
        });
      }
    );
  }
);

// Route::: PUT /wallet/:wallet_id/expenses/:expenses_id
// Desc::: Update expenses list by id
// Access::: Private
router.put(
  "/:wallet_id/expenses/:expenses_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Wallet.findById(req.params.wallet_id, (err, wallet) => {
      if (err) throw err;

      wallet.expenses.forEach(list => {
        if (list._id == req.params.expenses_id) {
          list.type = req.body.type;
          list.date = req.body.date;
          req.body.note ? (list.note = req.body.note) : (list.note = "");
          list.amount = req.body.amount;
        }
      });

      wallet.save((err, newWallet) => {
        if (err) throw err;
        return res.status(400).json(newWallet);
      });
    });
  }
);

// Route::: DELETE /wallet/:wallet_id/expenses/:expenses_id
// Desc::: Delete expenses list by id
// Access::: Private
router.delete(
  "/:wallet_id/expenses/:expenses_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Wallet.findById(req.params.wallet_id, (err, wallet) => {
      if (err) throw err;

      wallet.expenses = wallet.expenses.filter(
        list => list._id != req.params.expenses_id
      );

      wallet.save((err, newWallet) => {
        if (err) throw err;
        return res.status(400).json(newWallet);
      });
    });
  }
);

// Route::: POST /wallet/:wallet_id/income
// Desc::: Create income list
// Access::: Private
router.post(
  "/:wallet_id/income",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newIncome = req.body;

    Wallet.findByIdAndUpdate(
      req.params.wallet_id,
      { $push: { income: newIncome } },
      { new: true },
      (err, wallet) => {
        if (err) throw err;
        return res.status(400).json({
          wallet
        });
      }
    );
  }
);

// Route::: PUT /wallet/:wallet_id/income/:income_id
// Desc::: Update income list by id
// Access::: Private
router.put(
  "/:wallet_id/income/:income_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Wallet.findById(req.params.wallet_id, (err, wallet) => {
      if (err) throw err;

      wallet.income.forEach(list => {
        if (list._id == req.params.income_id) {
          list.type = req.body.type;
          list.date = req.body.date;
          req.body.note ? (list.note = req.body.note) : (list.note = "");
          list.amount = req.body.amount;
        }
      });

      wallet.save((err, newWallet) => {
        if (err) throw err;
        return res.status(400).json(newWallet);
      });
    });
  }
);

// Route::: DELETE /wallet/:wallet_id/income/:income_id
// Desc::: Delete income list by id
// Access::: Private
router.delete(
  "/:wallet_id/income/:income_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTransaction(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Wallet.findById(req.params.wallet_id, (err, wallet) => {
      if (err) throw err;

      wallet.income = wallet.income.filter(
        list => list._id != req.params.income_id
      );

      wallet.save((err, newWallet) => {
        if (err) throw err;
        return res.status(400).json(newWallet);
      });
    });
  }
);

module.exports = router;
